﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="_demo" Type="Folder">
		<Item Name="OOSpecTest.vi" Type="VI" URL="../OOSpecTest.vi"/>
		<Item Name="SimulationSpecTest.vi" Type="VI" URL="../SimulationSpecTest.vi"/>
	</Item>
	<Item Name="InheritBase" Type="Folder">
		<Item Name="RealSpec" Type="Folder">
			<Item Name="OOSpec" Type="Folder">
				<Item Name="SpecBaseOO.lvclass" Type="LVClass" URL="../SpecBase/RealSpec/OOSpec/SpecBaseOO.lvclass"/>
				<Item Name="SpecBaseOOHR2000+.lvclass" Type="LVClass" URL="../SpecBase/RealSpec/OOSpec/HR2000+/SpecBaseOOHR2000+.lvclass"/>
				<Item Name="SpecBaseOOHR4000.lvclass" Type="LVClass" URL="../SpecBase/RealSpec/OOSpec/HR4000/SpecBaseOOHR4000.lvclass"/>
				<Item Name="SpecBaseOOMaya-Pro.lvclass" Type="LVClass" URL="../SpecBase/RealSpec/OOSpec/Maya-Pro/SpecBaseOOMaya-Pro.lvclass"/>
			</Item>
			<Item Name="SpecBaseReal.lvclass" Type="LVClass" URL="../SpecBase/RealSpec/SpecBaseReal.lvclass"/>
		</Item>
		<Item Name="SimulationSpec" Type="Folder">
			<Item Name="SpecBaseSimulation.lvclass" Type="LVClass" URL="../SpecBase/SimulationSpec/SpecBaseSimulation.lvclass"/>
			<Item Name="SpecBaseSimulationCalculated.lvclass" Type="LVClass" URL="../SpecBase/SimulationSpec/SpecBaseSimulationCalculated.lvclass"/>
			<Item Name="SpecBaseSimulationOffline.lvclass" Type="LVClass" URL="../SpecBase/SimulationSpec/SpecBaseSimulationOffline.lvclass"/>
		</Item>
	</Item>
	<Item Name="InheritConfig" Type="Folder">
		<Item Name="RealConfig" Type="Folder">
			<Item Name="SpecConfigureOO.lvclass" Type="LVClass" URL="../InheritConfig/RealConfig/OceanOptics/SpecConfigureOO.lvclass"/>
			<Item Name="SpecConfigureOOHR2000+.lvclass" Type="LVClass" URL="../InheritConfig/RealConfig/OceanOptics/HR2000+/SpecConfigureOOHR2000+.lvclass"/>
			<Item Name="SpecConfigureOOHR4000.lvclass" Type="LVClass" URL="../InheritConfig/RealConfig/OceanOptics/HR4000/SpecConfigureOOHR4000.lvclass"/>
			<Item Name="SpecConfigureOOMaya-Pro.lvclass" Type="LVClass" URL="../InheritConfig/RealConfig/OceanOptics/Maya-Pro/SpecConfigureOOMaya-Pro.lvclass"/>
			<Item Name="SpecConfigureReal.lvclass" Type="LVClass" URL="../InheritConfig/RealConfig/SpecConfigureReal.lvclass"/>
		</Item>
		<Item Name="SimulationConfig" Type="Folder">
			<Item Name="SpecConfigureSimulation.lvclass" Type="LVClass" URL="../InheritConfig/SpecConfigureSimulation.lvclass"/>
			<Item Name="SpecConfigureSimulationCalculated.lvclass" Type="LVClass" URL="../InheritConfig/SimulationConfigCalculated/SpecConfigureSimulationCalculated.lvclass"/>
			<Item Name="SpecConfigureSimulationOffline.lvclass" Type="LVClass" URL="../InheritConfig/SimulationConfig/SpecConfigureSimulationOffline.lvclass"/>
		</Item>
	</Item>
	<Item Name="Force Close.vi" Type="VI" URL="../Force Close.vi"/>
	<Item Name="SpecBase.lvclass" Type="LVClass" URL="../SpecBase/SpecBase.lvclass"/>
	<Item Name="SpecConfigure.lvclass" Type="LVClass" URL="../SpecConfigure/SpecConfigure.lvclass"/>
</Library>
